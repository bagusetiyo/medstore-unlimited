# Changelog MedStore Unlimited

- Added   : untuk penambahan fitur.
- Changed : untuk perubahan pada menu yang sudah ada atau optimasi.
- Removed : untuk menghapus fitur.
- Fixed   : untuk perbaikan bug atau error.

# [3.3.0.16] - 2020-04-09
## Menu Pembelian Obat
- **Fixed :**
	1. Persentase diskon tidak otomatis berubah ketika pindah ke nominal diskon.

# [3.3.0.15] - 2019-09-11
## Menu Pembenahan Data
- **Changed :**
	1. Tambah fungsi pembenahan data mutasi double.

# [3.3.0.14] - 2019-03-13
## Menu Pembelian Obat
- **Changed :**
	1. Ada peringatan ketika tanggal faktur yang dipilih lebih dari tanggal hari ini.
## Menu Retur Pembelian Obat
- **Changed :**
	1. Ada peringatan ketika tanggal retur yang dipilih lebih dari tanggal hari ini.
## Menu Penjualan Obat
- **Changed :**
	1. Ada peringatan ketika tanggal transaksi yang dipilih lebih dari tanggal hari ini.
## Menu Retur Penjualan Obat
- **Changed :**
	1. Ada peringatan ketika tanggal retur yang dipilih lebih dari tanggal hari ini.
## Menu Mutasi Obat
- **Changed :**
	1. Ada peringatan ketika tanggal retur yang dipilih lebih dari tanggal hari ini.
	2. Ada tanda warna merah pada data mutasi yang tanggal mutasi nya tidak sesuai dengan tanggal pada saat melakukan mutasi pada saat dicetak kartu stok nya.
## Menu Salinan Resep
- **Changed :**
	1. Ada peringatan ketika tanggal resep dan tanggal pembuatan resep yang dipilih lebih dari tanggal hari ini.
## Menu Laporan Mutasi Obat
- **Changed :**
	1. Ada tanda warna merah pada data mutasi yang tanggal mutasi nya tidak sesuai dengan tanggal pada saat melakukan mutasi di hasil cetak kartu stok.
	2. Ada tanda warna merah pada data mutasi yang tanggal mutasi nya tidak sesuai dengan tanggal pada saat melakukan mutasi di hasil cetak laporan mutasi.
	
# [3.3.0.13] - 2019-01-18
## Menu Pembenahan Data
- **Fixed :**
	1. Fix data junal keuangan tidak terhapus ketika pilihan "SEMUA DATA TRANSAKSI, DATA STOK DAN HARGA OBAT TETAP".
	2. Fix data junal keuangan tidak terhapus ketika pilihan "SEMUA DATA TRANSAKSI, DATA STOK DAN HARGA OBAT KOSONG".

# [3.3.0.12] - 2018-12-28
## Menu Penjualan Obat
- **Changed :**
	1. Harga Jual tidak bisa diubah.
## Menu Mutasi Obat
- **Added :**
	1. Tambah informasi satuan obat.
## Menu Pembayaran Barang Konsinyasi
- **Added :**
	1. Tambah informasi satuan obat.
## Menu Laporan Stok Opname
- **Added :**
	1. Tambah informasi satuan obat.
## Menu Laporan History Harga Obat
- **Added :**
	1. Tambah informasi satuan obat.
## Menu Laporan Obat Expired Dan Obat Stok Habis 
- **Added :**
	1. Tambah informasi satuan obat.
## Menu Laporan Obat Hilang
- **Added :**
	1. Tambah informasi satuan obat.
## Menu Laporan Mutasi Obat
- **Added :**
	1. Tambah informasi satuan obat.
## Menu Laporan Pembelian Obat
- **Added :**
	1. Tambah informasi satuan obat.
## Menu Laporan Retur Pembelian Obat
- **Added :**
	1. Tambah informasi satuan obat.
## Menu Laporan Penjualan Obat
- **Added :**
	1. Tambah informasi satuan obat.
## Menu Laporan Retur Penjualan Obat
- **Added :**
	1. Tambah informasi satuan obat.
## Menu Laporan Obat Konsinyasi
- **Added :**
	1. Tambah informasi satuan obat.
## Menu Laporan Pembayaran Konsinyasi
- **Added :**
	1. Tambah informasi satuan obat.

# [3.3.0.11] - 2018-05-04
## Menu Laporan Stok Opname
- **Changed :**
	1. Ubah urutan tampil data berdasarkan tanggal proses stok opname.

# [3.3.0.10] - 2018-04-13
## Menu Registrasi
- **Changed :**
	1. Kode lisensi ditambah identitas nama apotek.

# [3.3.0.9] - 2018-01-05
## Menu Pembayaran Konsinyasi
- **Changed :**
	1. Tidak bisa membayar data obat konsinyasi ketika jumlah yang harus dibayar = 0.
	2. Hanya bisa melakukan retur data obat konsinyasi ketika stok = 0.
- **Fixed :**
	1. Fix ketika dilakukan retur obat sebagian, data obat langsung hilang.
## Menu Laporan Penjualan Obat
- **Fixed :**
	1. Fix salah menampilkan total perhitungan ketika dilakukan pencarian data obat.
## Menu Main Menu
- **Fixed :**
	1. Fix gagal memilih shift yang aktif setelah login.

# [3.3.0.8] - 2018-01-03
## Menu Identitas
- **Fixed :** 
	1. Fix ubah logo identitas.